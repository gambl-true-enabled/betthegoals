package com.tarokumage.betthegoals

import android.content.Context
import android.webkit.JavascriptInterface

private const val GOAL_TABLE = "com.goal.table.AR"
private const val GOAL_ARGS = "com.goal.value.RA"

fun Context.link(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(GOAL_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(GOAL_ARGS, deepArgs).apply()
}

fun Context.args(): String? {
    val sharedPreferences = getSharedPreferences(GOAL_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(GOAL_ARGS, null)
}

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}
