package com.tarokumage.betthegoals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.screen_2.*

class GoalsFragment : Fragment() {

    private var count1 = 0
    private var count2 = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        edit_1.setOnClickListener {
            (activity as GoalsActivity).startEditFragment()
        }
        plus_1.setOnClickListener {
            if (count1 < 99) {
                count1++
                text_6.text = count1.toString()
            }
        }
        plus_2.setOnClickListener {
            if (count2 < 99) {
                count2++
                text_7.text = count2.toString()
            }
        }
        minus_1.setOnClickListener {
            if (count1 > 0) {
                count1--
                text_6.text = count1.toString()
            }
        }
        minus_2.setOnClickListener {
            if (count2 > 0) {
                count2--
                text_7.text = count2.toString()
            }
        }
        text_6.text = count1.toString()
        text_7.text = count2.toString()
        elipse_2.setOnClickListener {
            count1 = 0
            count2 = 0
            text_6.text = count1.toString()
            text_7.text = count2.toString()
        }
        text_4.text = (activity as GoalsActivity).team1
        text_5.text = (activity as GoalsActivity).team2
        onboard_text_5.text = ((activity as GoalsActivity).bet + "$")
    }
}