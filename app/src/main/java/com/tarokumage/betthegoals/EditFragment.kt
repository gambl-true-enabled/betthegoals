package com.tarokumage.betthegoals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.screen_3.*

class EditFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.screen_3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arrow_1.setOnClickListener {
            (activity as GoalsActivity).onBackFragment()
        }
        elipse_3.setOnClickListener {
            if (textfield.text.toString().trim().isNotEmpty() && textfield_2.text.toString().trim().isNotEmpty() && textfield_3.text.toString().trim().isNotEmpty()) {
                (activity as GoalsActivity).saveInfo(
                    textfield.text.toString(),
                    textfield_2.text.toString(),
                    textfield_3.text.toString()
                )
            } else {
                Toast.makeText(context, "Заполните все поля", Toast.LENGTH_SHORT).show()
            }
        }
    }
}