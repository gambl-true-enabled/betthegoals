package com.tarokumage.betthegoals

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class GoalsActivity : AppCompatActivity() {

    var team1 = "team1"
    var team2 = "team2"
    var bet = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_goals)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                PreviewFragment()
            ).commit()
    }

    fun startMainFragment() {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                GoalsFragment()
            ).commit()
    }

    fun startEditFragment() {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                EditFragment()
            ).addToBackStack(null).commit()
    }

    fun onBackFragment() {
        supportFragmentManager.popBackStack()
    }

    fun saveInfo(
        team1: String,
        team2: String,
        bet: String
    ) {
        this.team1 = team1
        this.team2 = team2
        this.bet = bet
        supportFragmentManager.popBackStack()
    }
}
